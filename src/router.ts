import { createRouter, createWebHistory } from "vue-router"

import { store } from "./store"
import Browse from "./views/Browse.vue"
import Login from "./views/Login.vue"

export const router = createRouter({
  history: createWebHistory(),
  routes: [
    { name: "Browse", path: "/:path*", component: Browse },
    { name: "Login", path: "/login", component: Login }
  ]
})

// Load path before each route navigation
router.beforeEach((to, _, next) => {
  if (!store.state.app.auth && to.name !== "Login") {
    next({ name: "Login", query: { redirect: to.path } })
  } else {
    next()
  }

  if (store.state.app.auth && to.name !== "Login") {
    store.dispatch({ type: "loadPath", path: to.params.path || [] })
  }
})

import { Item } from "./item"

const testCases = [
  {
    json: {
      name: "Do Futuro",
      type: "directory",
      mtime: "Thu, 10 Oct 2019 10:42:59 GMT"
    },
    expected: {
      humanSize: "-",
      download: "https://test.example.com/api/Do%20Futuro/",
      kind: "directory"
    }
  },
  {
    json: {
      name: "Do.Futuro.mkv",
      type: "directory",
      mtime: "Sat, 02 Jan 2021 21:31:01 GMT"
    },
    expected: {
      humanSize: "-",
      download: "https://test.example.com/api/Do.Futuro.mkv/",
      kind: "directory"
    }
  },
  {
    json: {
      name: "Do.Futuro™.mkv",
      type: "file",
      mtime: "Sat, 02 May 2020 20:00:02 GMT",
      size: 1234567890
    },
    expected: {
      humanSize: "1.1G",
      download: "https://test.example.com/api/Do.Futuro%E2%84%A2.mkv",
      kind: "video"
    }
  },
  {
    json: {
      name: "Do.Futuro™.flac",
      type: "file",
      mtime: "Sat, 02 May 2020 20:00:02 GMT",
      size: 1234567890
    },
    expected: {
      humanSize: "1.1G",
      download: "https://test.example.com/api/Do.Futuro%E2%84%A2.flac",
      kind: "audio"
    }
  }
]

describe("item.ts", () => {
  testCases.forEach((tc) => {
    const item = new Item(tc.json as Item)

    const state = {
      app: { config: { api: "https://test.example.com/api" } },
      browse: { path: [] }
    }

    it(`should create from json with '${tc.json.name}'`, () => {
      expect(item.name).toBe(tc.json.name)
      expect(item.size).toBe(tc.json.size)
      expect(item.type).toBe(tc.json.type)
      expect(item.mtime).toBe(tc.json.mtime)
    })

    it(`should have valid humanSize with '${tc.json.name}'`, () => {
      expect(item.humanSize()).toBe(tc.expected.humanSize)
    })

    it(`should have valid fetchURL with '${tc.json.name}'`, () => {
      //@ts-ignore
      expect(item.fetchURL(state)).toBe(tc.expected.download)
    })

    it(`should have valid kind with '${tc.json.name}'`, () => {
      expect(item.kind).toBe(tc.expected.kind)
    })
  })
})

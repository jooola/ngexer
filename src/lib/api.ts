import { store } from "../store"
import { Toast, ToastKind } from "./toast"
import { Item } from "./item"

const request = (url: string, options?: RequestInit) => {
  return fetch(url, {
    headers: { ...store.getters.authHeader },
    ...options
  })
}

export const fetchPath = async (path: string): Promise<Item[] | undefined> => {
  try {
    const response = await request(path)

    if (response.status == 401 || response.status == 403) {
      Toast.send(ToastKind.WARNING, "Unauthorized access!")
      store.dispatch({ type: "logout" })
      return
    } else if (response.status == 404) {
      Toast.send(ToastKind.WARNING, response.statusText)
      return
    }

    return response.json()
  } catch (error) {
    Toast.send(ToastKind.ERROR, "Network error!")

    // eslint-disable-next-line
    console.error(error)

    return Promise.reject(error)
  }
}

export const authenticate = async (path: string): Promise<boolean> => {
  try {
    const response = await request(path, { method: "HEAD" })

    return response.status === 200
  } catch (error) {
    // eslint-disable-next-line
    console.error(error)

    return false
  }
}

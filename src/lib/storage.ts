export const load = <T>(key: string): T | undefined => {
  const blob = localStorage.getItem(key)
  if (blob) return JSON.parse(blob)
}

export const save = <T>(key: string, value: T): void => {
  localStorage.setItem(key, JSON.stringify(value))
}

export const flush = (key: string): void => {
  localStorage.removeItem(key)
}

import trim from "lodash/trim"
import trimEnd from "lodash/trimEnd"

interface URLJoinOptions {
  encode?: boolean
  endSlash?: boolean
}

export const urlJoin = (
  items: string[],
  options: URLJoinOptions = {}
): string => {
  if (items.length <= 1) return items[0]

  const { encode = false, endSlash = false } = options

  const base = trimEnd(items.shift(), "/")

  const path = items.map(
    encode
      ? (item: string) => encodeURIComponent(trim(item, "/"))
      : (item: string) => trim(item, "/")
  )

  const url = [base, ...path].join("/")
  return endSlash ? url + "/" : url
}

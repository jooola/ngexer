import { Item } from "./item"

export enum OrderBy {
  NAME = "name",
  MTIME = "mtime",
  SIZE = "size"
}

const sortFn = {
  [OrderBy.NAME]: (a: Item, b: Item): number => {
    if (a.isDirectory() && !b.isDirectory()) {
      return -1
    } else if (!a.isDirectory() && b.isDirectory()) {
      return 1
    }
    return a.name.localeCompare(b.name)
  },
  [OrderBy.MTIME]: (a: Item, b: Item): number => {
    return new Date(a.mtime).valueOf() - new Date(b.mtime).valueOf()
  },
  [OrderBy.SIZE]: (a: Item, b: Item): number => {
    return (a.size || 0) - (b.size || 0)
  }
}

export const getSortFn = (orderBy: OrderBy): ((a: Item, b: Item) => number) => {
  return sortFn[orderBy]
}

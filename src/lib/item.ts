import { format as formatMtime } from "timeago.js"
import { urlJoin } from "./url"
import { State } from "../store"

export enum ItemKind {
  DIRECTORY = "directory",
  AUDIO = "audio",
  VIDEO = "video",
  PICTURE = "picture",
  OTHER = "other"
}

const findItemKind = (item: Item): ItemKind => {
  if (item.type === "directory") return ItemKind.DIRECTORY

  for (const kind in extensions) {
    for (const extension in extensions[kind]) {
      if (item.name.indexOf(extensions[kind][extension]) !== -1) {
        return kind as ItemKind
      }
    }
  }

  return ItemKind.OTHER
}

const extensions: { [key: string]: string[] } = {
  [ItemKind.AUDIO]: [".flac", ".mp3", ".ogg", ".dsf"],
  [ItemKind.VIDEO]: [".mp4", ".mkv", ".ts", ".avi", ".m4v"],
  [ItemKind.PICTURE]: [".jpg", ".png", ".svg"]
}

const sizeUnits = ["", "K", "M", "G", "T"]
const formatSize = (size?: number): string => {
  if (size !== undefined) {
    const i = Math.min(
      Math.floor(Math.log(size) / Math.log(1024)),
      sizeUnits.length - 1
    )
    const value = (size / Math.pow(1024, i)).toFixed(1)
    const unit = sizeUnits[i]

    return `${value}${unit}`
  }
  return "-"
}

export interface Item {
  name: string
  type: string
  mtime: string
  size?: number
}

export class Item {
  name: string
  type: string
  mtime: string
  size?: number

  kind: ItemKind

  constructor({ name, type, mtime, size }: Item) {
    this.name = name
    this.type = type
    this.mtime = mtime
    this.size = size
    this.kind = findItemKind(this)
  }

  isDirectory(): boolean {
    return this.kind === ItemKind.DIRECTORY
  }
  isPlayable(): boolean {
    return this.kind === ItemKind.AUDIO || this.kind === ItemKind.VIDEO
  }

  humanMtime(): string {
    return formatMtime(this.mtime)
  }
  humanSize(): string {
    return formatSize(this.size)
  }

  fetchURL(state: State): string {
    if (!state.app.config) return ""
    return urlJoin([state.app.config.api, ...state.browse.path, this.name], {
      encode: true,
      endSlash: this.isDirectory()
    })
  }
}

export interface Config {
  api: string
  prefetch: boolean
}

export const fetchConfig = async (): Promise<Config> => {
  const response = await fetch("/config.json")
  return response.json()
}

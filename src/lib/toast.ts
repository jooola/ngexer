import { store } from "../store"

export enum ToastKind {
  INFO = "info",
  WARNING = "warning",
  ERROR = "error"
}

let toastCounter = 0

export class Toast {
  id: number

  constructor(public kind: ToastKind, public message: string) {
    this.id = ++toastCounter
  }

  static send(kind: ToastKind, message: string): void {
    store.dispatch({ type: "sendToast", toast: new Toast(kind, message) })
  }
}

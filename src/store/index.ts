import { createStore, useStore as baseUseStore, Store as BaseStore } from "vuex"
import { InjectionKey } from "vue"

import app, { State as AppState } from "./app"
import browse, { State as BrowseState } from "./browse"
import toaster, { State as ToasterState } from "./toaster"

export interface State {
  app: AppState
  browse: BrowseState
  toaster: ToasterState
}

export type Store = BaseStore<State>
export const key: InjectionKey<BaseStore<State>> = Symbol()

export const useStore = (): BaseStore<State> => {
  return baseUseStore(key)
}

export const store = createStore<State>({
  modules: {
    app,
    browse,
    toaster
  }
})

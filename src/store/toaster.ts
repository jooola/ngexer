import { MutationTree, ActionTree } from "vuex"
import { Toast } from "../lib/toast"

import { State as RootState } from "."

export interface State {
  toasts: Toast[]
}

const state: State = {
  toasts: []
}

const mutations: MutationTree<State> = {
  pushToast: (state, payload: { toast: Toast }) => {
    state.toasts.push(payload.toast)
  },

  removeToast: (state, payload: { id: number }) => {
    state.toasts = state.toasts.filter((t) => t.id !== payload.id)
  }
}

const actions: ActionTree<State, RootState> = {
  sendToast: ({ commit }, payload: { toast: Toast }) => {
    commit({ type: "pushToast", toast: payload.toast })
    setTimeout(
      () => commit({ type: "removeToast", id: payload.toast.id }),
      5000
    )
  }
}

export default {
  state,
  mutations,
  actions
}

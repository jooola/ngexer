import { MutationTree, ActionTree, GetterTree } from "vuex"

import { load, save, flush } from "../lib/storage"
import { fetchConfig, Config } from "../lib/config"

import { State as RootState } from "."
import { authenticate } from "../lib/api"
import { urlJoin } from "../lib/url"

export interface Auth {
  username: string
  password: string
}

export interface State {
  config?: Config
  auth?: Auth
}

const state: State = {
  config: load<Config>("config"),
  auth: load<Auth>("auth")
}

const mutations: MutationTree<State> = {
  setConfig: (state, payload: { config?: Config }) => {
    state.config = payload.config
    if (payload.config) save("config", payload.config)
    else flush("config")
  },

  setAuth: (state, payload: { auth?: Auth }) => {
    state.auth = payload.auth
    if (payload.auth) save("auth", payload.auth)
    else flush("auth")
  }
}

const actions: ActionTree<State, RootState> = {
  loadConfig: async ({ commit }) => {
    const config = await fetchConfig()
    commit({ type: "setConfig", config })
  },

  login: async ({ commit, state }, payload: { auth: Auth }) => {
    if (state.config) {
      commit({ type: "setAuth", auth: payload.auth })
      const valid = await authenticate(
        urlJoin([state.config.api], { endSlash: true })
      )
      if (!valid) {
        commit({ type: "setAuth" })
      }
      return valid
    }
    return false
  },

  logout: ({ commit }) => {
    commit({ type: "setAuth" })
  }
}

interface AuthHeader {
  Authorization?: string
}

const getters: GetterTree<State, RootState> = {
  authHeader: (state): AuthHeader => {
    if (state.auth) {
      const token = btoa(`${state.auth.username}:${state.auth.password}`)
      return { Authorization: "Basic " + token }
    }
    return {}
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}

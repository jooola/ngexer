import { MutationTree, ActionTree, GetterTree } from "vuex"

import { Item } from "../lib/item"
import { OrderBy, getSortFn } from "../lib/items"
import { fetchPath } from "../lib/api"

import { State as RootState } from "."
import { urlJoin } from "../lib/url"

export interface State {
  loading: boolean
  items: Item[]
  fetched: string[]
  path: string[]
  orderBy: OrderBy
  orderReverse: boolean
}

const state: State = {
  loading: false,
  items: [],
  fetched: [],
  path: [],
  orderBy: OrderBy.NAME,
  orderReverse: false
}

const mutations: MutationTree<State> = {
  setLoading: (state, payload: { loading: boolean }) => {
    state.loading = payload.loading
  },

  setItems: (state, payload: { items: Item[] }) => {
    state.items = payload.items
  },

  sortItems: (state) => {
    const sorted = state.items.sort(getSortFn(state.orderBy))
    state.items = state.orderReverse ? sorted.reverse() : sorted
  },

  setOrderBy: (state, payload: { orderBy: OrderBy }) => {
    state.orderBy = payload.orderBy
  },

  setOrderReverse: (state, payload: { orderReverse: boolean }) => {
    state.orderReverse = payload.orderReverse
  },

  setPath: (state, payload: { path: string | string[] }) => {
    const path = payload.path
    state.path = Object.assign([], Array.isArray(path) ? path : path.split("/"))
  },

  pushFetched: (state, payload: { name: string }) => {
    state.fetched.push(payload.name)
  },

  flushFetched: (state) => {
    state.fetched = []
  }
}

const actions: ActionTree<State, RootState> = {
  loadPath: async (
    { state, rootState, commit },
    payload: { path: string | string[] }
  ) => {
    commit({ type: "setLoading", loading: true })
    commit({ type: "setPath", path: payload.path })
    commit({ type: "flushFetched" })

    if (rootState.app.config) {
      const items = await fetchPath(
        urlJoin([rootState.app.config.api, ...state.path], {
          encode: true,
          endSlash: true
        })
      )

      if (items) {
        commit({
          type: "setItems",
          items: items.map((item: Item) => new Item(item))
        })
        commit({ type: "sortItems" })
        commit({ type: "setLoading", loading: false })
      }
    }
  },

  prefetch: async ({ state, rootState, commit }, payload: { item: Item }) => {
    if (
      rootState.app.config?.prefetch &&
      payload.item.isDirectory() &&
      !state.fetched.includes(payload.item.name)
    ) {
      await fetchPath(payload.item.fetchURL(rootState))
      commit({ type: "pushFetched", name: payload.item.name })
    }
  },

  sortItems: (
    { commit },
    { orderBy, orderReverse }: { orderBy?: OrderBy; orderReverse?: boolean }
  ) => {
    let refresh = false

    if (orderBy !== undefined) {
      commit({ type: "setOrderBy", orderBy })
      refresh = true
    }
    if (orderReverse !== undefined) {
      commit({ type: "setOrderReverse", orderReverse })
      refresh = true
    }

    if (refresh) commit({ type: "sortItems" })
  }
}

const getters: GetterTree<State, RootState> = {
  dirname: (state) => {
    return Object.assign([], state.path).pop() || "/"
  },

  parent: (state) => {
    return state.path.slice(0, state.path.length - 1)
  },

  playableItems: (state) => {
    return state.items.filter((item) => item.isPlayable())
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
